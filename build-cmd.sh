#!/bin/bash

# turn on verbose debugging output for teamcity logs.
set -x

# make errors fatal
set -e

if [ -z "$AUTOBUILD" ] ; then
    echo 'AUTOBUILD not set' 1>&2
    exit 1
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autobuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

# This version helps define which archive will be used to extract files from
# Should match the viewer from which the plugins and launcher were extracted from
SLVOICE_VERSION=3.2.0002.10426

top="$(pwd)"
stage="$top"

case "$AUTOBUILD_PLATFORM" in
"windows")
    python -c "import zipfile; zipfile.ZipFile('../slvoice-${SLPLUGINS_VERSION}-Release-win32.zip').extractall()"
;;
"darwin")
    tar -xvf ../slvoice-${SLVOICE_VERSION}-Release-darwin.tar.bz2
;;
"linux")
     SLVOICE_VERSION=3.2.0002.10426.298329
    tar -xvf ../slvoice-${SLVOICE_VERSION}-linux-20160717.tar.bz2
    cp -a ../libvivoxoal.so.1 "${stage}"/lib/release
    mkdir -p "${stage}/lib/release/32bit-compat"
	cd "${stage}/lib/release/32bit-compat"
    cp -a ~/3p-slvoice/32bit-compat/* .

 
;;
"linux64")
    SLVOICE_VERSION=3.2.0002.10426.298329.193031910
    tar -xvf ../slvoice-${SLVOICE_VERSION}-linux64-193031910.tar.bz2
    cp -a ../libvivoxoal.so.1 "${stage}"/lib/release
    mkdir -p "${stage}/lib/release/32bit-compat"
	cd "${stage}/lib/release/32bit-compat"
    cp -a ~/hg_3p-slvoice/32bit-compat/* .

 
;;
esac
# Create the version number file from variable in this file
version=${SLVOICE_VERSION}
build=${AUTOBUILD_BUILD_ID:=0}
echo "${version}.${build}" > "${stage}/VERSION.txt"

# Copy over other files
mkdir -p "$stage/LICENSES"
cp "$top/../slvoice.txt" "$stage/LICENSES"


